<?php

namespace App\Modules\Admin\Http\Controllers;

use App\Classes\Base;
use App\Classes\DynamicModel;
use App\Http\Controllers\Controller;
use App\Modules\Admin\Models\Modules;
use App\Modules\Admin\Models\Plugins;
use App\Modules\Admin\Models\Right;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Response;
use Storage;
use Config;
use Illuminate\Console\Command;

class ComponentController extends Controller
{
    public function __construct(Request $request)
    {
        parent::__construct();

        $this->request  = $request->all();
        $this->requests = $request;
        $this->dynamic  = new DynamicModel();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function get_loader_ordering()
    {
        $id                 = $this->request['id'] ?? 0;
        $where[]            = ['product.active', 1];
        $group              = 'id';
        $data               = [];
        $data['status_app'] = ['Новая заявка', 'В обработке', 'Заказ отправлен', 'Закрыт', 'Отказ'];


        $data['type_measure'] = [
            0 => 'Шт',
            1 => 'кг', // Грамм
            2 => 'л', // Мл
            3 => 'Шт/грамм',
            4 => 'Шт/мл',
        ];

        $data['statuses'] = $this->dynamic->t('applications_time')
            ->where(['parent_id' => $id])
            ->get();

        $data['carts'] = $this->dynamic->t('product')
            ->where($where)

            ->join('applications_ordering', function($join) use($id)
            {
                $join->type = 'RIGHT';
                $join->on('product.id', '=','applications_ordering.product_id')
                    ->where('applications_ordering.applications_id', '=', $id);
            })

            ->join('files', function($join)
            {
                $join->type = 'LEFT OUTER';
                $join->on('product.id', '=','files.id_album')
                    ->where('files.name_table', '=', 'product')
                    ->where('files.main', '=', 1);
            })

            ->join('menu_shop', function($join)
            {
                $join->type = 'LEFT OUTER';
                $join->on('product.cat', '=','menu_shop.id');
            })

            ->select('product.*', 'files.file', 'files.crop', 'menu_shop.name as cat_parent', 'applications_ordering.weights')
            ->groupBy('product.id')
            ->orderBy('product.' . $group, 'DESC')
            ->get()
            ->toArray();

        return Base::view("admin::components.ordering", $data);
    }

    public function statistics()
    {

        $data= $this->dynamic->t('applications_time')
            ->select('id', 'item_name','quantity', 'created_at')
            ->get()
            ->groupBy(function($val) {
                return Carbon::parse($val->created_at)->format('m');
            });

        return Base::view("admin::components.statistics", []);
    }
}