<?php

namespace App\Modules\Admin\Http\Controllers;

use App\Classes\Base;
use App\Modules\Admin\Models\Modules;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class SettingsController extends Controller
{
    public function __construct(Request $request)
    {
        parent::__construct();

        $this->modules = new Modules();
//        $this->plugins = new Plugins();

//        $this->dynamic = new DynamicModel();
        $this->base = new Base($request);

        $this->right = $this->base->right();
        $this->base->right_check();
    }

    /**
     * @return \Illuminate\View\View
     */
    public function getIndex()
    {
        try {


            return Base::view("admin::settings.index", [

            ]);
        } catch (\Exception $err){
            return Base::errorPage($err);
        }
    }
}
