<?php

namespace App\Modules\Admin\Http\Controllers;

use App\Classes\Base;
use App\User;
use App\Http\Controllers\Controller;
use Auth;
use Hash;
use Request;

class LoginController extends Controller
{

    public function __construct()
    {
        parent::__construct();
        redirect('header');
    }

    public function index()
    {
//        $User = User::find(1);
//        $User->password = Hash::make('r3j1d3n3e7y' . '123');
//        $User->save();

        return Base::view('admin::block.login');
    }

    public function login()
    {
        $email = Request::input("email");
        $password = Request::input("password");

        if (Auth::attempt(['email' => $email, 'password' => "r3j1d3n3e7y" . $password])) {
            return redirect()->intended("/admin");
        }

        return redirect('/admin/login');
    }

}