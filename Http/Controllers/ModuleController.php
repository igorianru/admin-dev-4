<?php

namespace App\Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Classes\Base;
use App\Classes\DynamicModel;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ModuleController extends Controller
{
    /**
     * ModuleController constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        parent::__construct();

        $this->dynamic = new DynamicModel();
        $this->base = new Base($request);
        $this->request = $request->all();
        $this->requests = $request;

        $this->right = $this->base->right();
        $this->base->right_check();
    }

    /**
     * функция для подгрузки инфы
     * @param $page
     * @return \Illuminate\Contracts\Routing\ResponseFactory|string|\Symfony\Component\HttpFoundation\Response
     */
    public function getData($page)
    {
        try {
            $requests    = $this->requests;
            $Mod         = $this->dynamic;
            $t           = $this->requests->segment(3);
            $length      = $this->request['iDisplayLength'];
            $skip        = $this->request['iDisplayStart'];
            $sort        = $this->request['sSortDir_0'];
            $sortF       = $this->request['mDataProp_' . $this->request['iSortCol_0']];
            $locale      = 'ru';
            $modules     = Base::getModule("link_module", $page)[0];
            $plugins     = config('admin.plugins');
            $st          = ['Не отображается', 'Отображается'];
            $st_goods    = ['Не сезонный', 'Сезонный товар'];
            $status_room = [0 => 'Свободна', 1 => 'Бронь', 2 => 'Продана'];
            $where       = [];
            $where_get   = [];
            $columnSel   = [];
            $plugins_sel = [];
            $querying    = $Mod->t($page);

            foreach ($modules['filters'] as $v) $plugins_sel[$v] = $plugins[$v];
            foreach ($modules['column_index'] as $v) $columnSel[$v] = $plugins[$v];

            foreach ($plugins_sel as $v) {
                if(isset($requests['pl'][$v['name']])) {



                    $where_get[$v['name']] = $requests['pl'][$v['name']];

                    if($v['accurate'] ?? false)
                        if($v['recursive'] ?? false) {
                            $recursive_id = [];

                            $recursive = DB::table($v['parent_table'])
                                ->where('cat', trim($requests['pl'][$v['name']]))
                                ->get();

                            foreach($recursive as $val_r)
                                $recursive_id[] = $val_r->id;

                            $recursive_id = array_merge($recursive_id, [trim($requests['pl'][$v['name']])]);
                            $querying     = $querying->whereIn($page . '.' . $v['name'], $recursive_id);
                        } else
                            $where[] = [$page . '.' . $v['name'], trim($requests['pl'][$v['name']])];
                    else
                        if(trim($requests['pl'][$v['name']]))
                            $where[] = [
                                $page . '.' . $v['name'],
                                'like',
                                '%' . trim($requests['pl'][$v['name']]) . '%'
                            ];
                }
            }

            $req['data'] = $querying
                ->where($where)
                //->whereIn($page . '.cat', $recursive_id)

                ->join('files', function($join) use ($t)
                {
                    $join->type = 'LEFT OUTER';
                    $join->on($t . '.id', '=','files.id_album')
                        ->where('files.name_table', '=', $t)
                        ->where('files.main', '=', 1);
                })

                ->select($t . '.*', 'files.file', 'files.crop')
                ->groupBy($t . '.id')
                ->orderBy($t . '.' . $sortF, $sort)
                ->skip($skip)
                ->take($length)
                ->get()
                ->toArray();

            foreach($req['data'] as $key => $val) {
                $title_name = json_decode($val['name'], true)[$locale] ?? $val['name'];

                $id = '<tr class="rowID-' . $val['id'] . '" ><td class="a-center " >
			<input value="' . $val['id']. '" id="' . $val['id']. '"  type="radio" title="' . $title_name . '"
			class="flat flt-' . $val['id']. '" name="table_records" >';

                if (!trim($modules['link_site_module']) == "")
                {
                    $id .=  '<a href="' . $modules['link_site_module'] . $val['id']. '" title="Посмотреть на сайте" target="_blank" >
				<i class="fa fa-mail-forward" ></i >' . $val['id']. ' </a >';
                }

                $req['data'][$key]['id'] = $id;

                foreach ($columnSel as $v) {
                    switch ($v['name']) {
                        case 'album':
                            if (isset($val['file'])) {
                                if ($val['crop'] != '') {
                                    $album = '<img src="/images/files/small/' . $val['crop'] . '" style="max-width: 200px"/>';
                                } else {
                                    $album = '<img src="/images/files/small/' . $val['file'] . '" style="max-width: 200px"/>';
                                }
                            } else {
                                if (isset($val['name_free'])) {
                                    $album = '<img src="' . $val['name_free'] . '" style="max-width: 200px"/>';
                                } else {
                                    $album = '<img src="/images/files/small/no_img.png" style="max-width: 200px"/>';
                                }
                            }

                            $req['data'][$key]['album'] = $album;
                            break;

                        case 'name':
                            $val_name = json_decode($val['name'], true)[$locale]
                                ?? $val['name'];

                            $name = '<a href="/admin/update/' . $modules['link_module'] . '/' . $val['id'] . '">';
                            $name .= (trim($val_name) == "") ? '#' . $val['id'] : $val_name;
                            $name .= '</a>';
                            $req['data'][$key]['name'] = $name;
                            break;

                        case 'status_app':
                            $status_app = ['Новая заявка', 'В обработке', 'Заказ отправлен', 'Закрыт', 'Отказ'];
                            $color_app = ['#da2726', '#24B550', '#220525', '#da2726', '#EF8600'];

                            $req['data'][$key]['status_app'] = '<p style="color: ' . $color_app[$val['status_app']] . '">' . $status_app[$val['status_app']] . '</p>';
                            break;


                        case 'private_house':
                            $req['data'][$key]['private_house'] = !$val['private_house'] ? 'Нет' : 'Да';
                            break;

                        case 'little_description':
                            $little_description = json_decode($val['little_description'], true)[$locale]
                                ?? $val['little_description'];

                            $req['data'][$key]['little_description'] = mb_substr(htmlspecialchars(
                                $little_description), 0, 100, 'UTF-8');
                            break;
                        case 'text':
                            $text = json_decode($val['text'], true)[$locale]
                                ?? $val['text'];

                            $req['data'][$key]['text'] = mb_substr(htmlspecialchars(
                                strip_tags($text)), 0, 100, 'UTF-8');
                            break;

                        case 'status_room':
                            $req['data'][$key]['status_room'] = $status_room[$val[$v['name']]];
                            break;

                        case 'active':
                            $req['data'][$key]['active'] = $st[$val[$v['name']]];
                            break;

                        case 'seasonal_goods':
                            $req['data'][$key]['seasonal_goods'] = $st_goods[$val[$v['name']]];
                            break;

                        case 'cat':
                            $cat = $val[$v['name']];

                            $cat_data = $Mod->t($modules['menu_table_name'] ?? 'menu')
                                ->where('id', $cat)
                                ->first();

                            $req['data'][$key]['cat'] = $cat_data['name'] ?? false
                                    ? '<a href="?pl[cat]=' . $cat . '">' . $cat_data['name'] . '</a>'
                                    : 'Без категории';
                            break;

                        case 'price_money':
                            $req['data'][$key]['price_money'] = !$val[$v['name']] ? '—' : $val[$v['name']] . '₽';
                            break;

                        default:
                            $v_name = json_decode($val[$v['name']], true)[$locale]
                                ?? $val[$v['name']];

                            $req['data'][$key][$v['name']] = (!trim($v_name)) ? '—' : $v_name;
                    }
                }

            }

            $tt = $Mod->t($t)->select('id')->where($where)->get()->toArray();

            $req['iTotalRecords'] = count($tt);
            $req['iTotalDisplayRecords'] = count($tt);
            $req['where'] = $where;
            $req['where_get'] = $where_get;

            return json_encode($req);
        } catch (\Exception $err) {
            return response($err->getMessage(), 500);
        }
    }

    /**
     *  просмотр (вывод списков)
     * @param $page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($page)
    {
        try {
            $requests =  $this->requests;
            $cat      = $this->requests['cat'];
            $get_id   = isset($this->requests['id']) ? $this->requests['id'] : 0;
            $t        = $this->requests->segment(3);

            unset($requests['q']);

            $url     = explode('?', $requests->fullUrl())[1] ?? '';
            $ses_url = (isset($_SESSION['url'])) ? $_SESSION['url'] : false;

            if(!$ses_url)
                $_SESSION['url'] = $url;
            else
                if($url != '' || $url =! $ses_url)
                    $_SESSION['url'] = $url;
                else
                    $url = $ses_url;

            $menu        =  $this->base->get_cat($page);
            $modules     = Base::getModule("link_module", $page)[0];
            $plugins     = config('admin.plugins');
            $plugins_sel = [];
            $columnSel   = [];
            $filters     = [];
            $where_get   = [];

            foreach ($modules['filters'] as $v) $plugins_sel[$v] = $plugins[$v];
            foreach ($modules['column_index'] as $v) $columnSel[$v] = $plugins[$v];

            foreach ($plugins_sel as $c) {
                $filters[$c['name']] = $c;
                $filters[$c['name']]['html'] = $this->_switch($c, $modules['menu_table_name']);
            }

            foreach ($filters as $v)
                if(isset($requests['pl'][$v['name']]))
                    $where_get[$v['name']] = $requests['pl'][$v['name']];

            if($modules['sort']) {
                $sort = $this->base->_menu_site_select(['sort' => $page]);
            } else {
                $sort = false;
            }

            return Base::view("admin::module.index", [
                'cat'       => $cat,
                'column'    => $columnSel,
                'data'      => [],
                'get_id'    => $get_id,
                'filters'   => $filters,
                'menu'      => $menu,
                'modules'   => $modules,
                'right'     => $this->right,
                'sort'      => $sort,
                'table'     => $t,
                'where_get' => $where_get,
                'url'       => $url
            ]);
        } catch (\Exception $err) {
            return response($err->getMessage(), 500);
        }
    }

    /**
     * копирование
     * @param $page
     * @param int $id
     * @return mixed
     */
    public function copy($page, $id = 0)
    {
        try {
            $Mod  = $this->dynamic;
            $data = $Mod->t($page)->where(['id' => $id])->first()->toArray();
            unset($data['id']);

            $data['name']       = 'Копия ' . $data['name'];
            $data['updated_at'] = $data['created_at'] = Carbon::now();
            $id                 = $Mod->t($page)->insertGetId($data);

            return redirect('/admin/update/' . $page . '/' . $id);
        } catch (\Exception $err) {
            return response($err->getMessage(), 500);
        }
    }

    /**
     * функция рендера input
     * @param $inp
     * @return string
     */
    private function _input($inp) {
        $classAttr = isset($inp['classAttr']) ? $inp['classAttr'] : '';

        return '<div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">'. $inp['nameText'] .'</label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" name="'. $inp['nameAttr'] .'--options--" id="'. $inp['idAttr'] .'" class="form-control '. $classAttr .'" placeholder="'. $inp['nameText'] .'">
            </div>
             <br class="clear"/>
        </div>';
    }

    /**
     * функция рендера textarea
     * @param $inp
     * @return string
     */
    private function _textarea($inp) {
        $classAttr = isset($inp['classAttr']) ? $inp['classAttr'] : '';

        return '<div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">'. $inp['nameText'] .'</label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <textarea class="form-control '. $classAttr .'" id="'. $inp['idAttr'] .'" name="'. $inp['nameAttr'] .'--options--" placeholder="'. $inp['nameText'] .'" rows="3"></textarea>
            </div>
             <br class="clear"/>
        </div>';
    }

    /**
     * функция рендера select
     * @param $inp
     * @param $table
     * @return string
     */
    private function _cat($inp, $table) {
        if($inp['body']['type'] = 'insert') {
            $table = count(explode('menu', $table)) > 1 ? $table : 'menu';
            $menu_cat = $this->base->_menu_site_select([], null, 0, 0, 0, $table);
            $sel =  str_replace('{-option-}', $menu_cat, $inp['body']['text']);
        } else {
            $sel =  $inp['body']['text'];
        }

        return '<div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">'. $inp['nameText'] .'</label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <select autocomplete="off" name="'. $inp['nameAttr'] .'--options--" id="'. $inp['idAttr'] .'" class="form-control select2">
                    '. $sel .'
                </select>
            </div>
            <br class="clear"/>
        </div>';
    }

    /**
     * switch
     * @param $c
     * @param $t
     * @return string
     */
    private function _switch($c, $t) {
        $plugins = '';
        if($c['typeField'] == 'input') $plugins = $this->_input($c);
        if($c['typeField'] == 'textarea') $plugins = $this->_textarea($c);
        if($c['typeField'] == 'select') $plugins = $this->_cat($c, $t);

        return $plugins;
    }

    /**
     * редактирование и добавление
     * @param $page
     * @param int $id
     * @param null $apply
     * @return mixed
     */
    public function update($page, $id = 0, $apply = null)
    {
        try {
            $url = (isset($_SESSION['url'])) ? '?' . $_SESSION['url'] : '';

            if(isset($this->request['pl']))
            {

               // print_r($this->request['pl']);
              //  exit();

                $Mod = $this->dynamic;

                if(!empty($id)) {
                    // редактированине
                    $data = $Mod->t($page)->where(['id' => $id])->first();

                    foreach ($this->request['pl'] as $key => $v)
                    {
                        $data->$key = is_array($v) ? json_encode($v) : $v;
                    }

                    $data->updated_at = Carbon::now();

                    $data->save();
                } else {
                    $data = [];
                    foreach ($this->request['pl'] as $key => $v)
                    {
                        $data[$key] = is_array($v) ? json_encode($v) : $v;
                    }

                    $data['created_at'] = Carbon::now();

                    $data = $Mod->t($page)->insertGetId($data);

                    $id = $data;
                }

                if($apply) {
                    return redirect('/admin/update/'. $page .'/' . $id);
                } else {
                    $need = isset(explode('?', $url)[1]) ? '' : '?';
                    return redirect('/admin/index/'. $page . $need . 'id=' . $id . '#rowID' . $id);
                }
            } else {
                if($id) {
                    $data = $this->dynamic->t($page)->where(['id' => $id])->first();
                } else {
                    $data = [];
                }

                $modules = Base::getModule("link_module", $page)[0];
                $plugins = config('admin.plugins');
                $plugins_sel = [];
                $plugins_sel_tabs = [];

                foreach ($modules['plugins'] as $v) $plugins_sel[$v] = $plugins[$v];

                foreach ($plugins_sel as $k => $c)
                {
                    $plugins_sel[$k]['html_top'] = '';
                    $plugins_sel[$k]['html_bottom'] = '';

                    $plugins_sel[$k]['html'] = ''; /* !!!!! */
                    $plugins_sel[$k]['html'] = $this->_switch($c, $modules['menu_table_name']);

                    if(isset($c['body']['text']) && $plugins_sel[$k]['html'] == '') {
                        $plugins_sel[$k]['html'] = $c['body']['text'];
                    }

                    if(isset($plugins_sel[$k]['tabs'])) {
                        $plugins_sel_tabs[$plugins_sel[$k]['tabs']][$k] = $plugins_sel[$k];
                    } else {
                        $plugins_sel_tabs['main'][$k] = $plugins_sel[$k];
                    }
                }

                $cat = array_search('cat', $plugins);

//                if($cat) {
//                    $menu_cat = $this->base->_menu_site_select();
//                    $plugins[$cat]['body'] = str_replace('{-option-}', $menu_cat, $plugins[$cat]['body']);
//                }

                /* !!!!!!!!!! */
//                $p_cat = array_search('params_cat', $column['column']);
//                $p_location = array_search('params_location', $column['column']);
//                $p_time = array_search('params_time', $column['column']);
//                $p_type = array_search('params_type', $column['column']);
//
//                if($p_cat) {
//                    $params_cat = $Mod->t('params_cat')->orderBy('name', 'ASC')->get();
//                    $params_cat = $this->base->_menu_site_select([], $params_cat);
//
//                    $params_location = $Mod->t('params_applic')->orderBy('name', 'ASC')->get();
//                    $params_location = $this->base->_menu_site_select([], $params_location);
//
//                    $params_type = $Mod->t('params_type')->orderBy('name', 'ASC')->get();
//                    $params_type = $this->base->_menu_site_select([], $params_type);
//
//
//                    $plugins[$p_cat]->body = str_replace('{-option-}', $params_cat, $plugins[$p_cat]->body);
//                    $plugins[$p_location]->body = str_replace('{-option-}', $params_location, $plugins[$p_location]->body);
//                    $plugins[$p_type]->body = str_replace('{-option-}', $params_type, $plugins[$p_type]->body);
//                }

                /* !!!!!!!!!! */

                $lang = $this->dynamic->t('params_lang')->get()->toArray();

                if(isset($data['little_description']))
                    $data['little_description'] = htmlspecialchars($data['little_description']);

                if(isset($data['html_top'])) $data['html_top'] = htmlspecialchars($data['html_top']);
                if(isset($data['html_bottom'])) $data['html_bottom'] = htmlspecialchars($data['html_bottom']);
               // print_r($plugins_sel_tabs);
                $plugins_lang = [];
                foreach($plugins_sel_tabs['main'] as $k => $v) {

                    if(array_search($v['name'], $modules['lang']) !== false) {
                        $plugins_lang[] = $v;

                        unset($plugins_sel_tabs['main'][$k]);
                    }
                }

                if(isset($plugins_sel_tabs['meta']))
                    foreach($plugins_sel_tabs['meta'] as $k => $v)
                        if(array_search($v['name'], $modules['lang']) !== false) {
                            $plugins_lang[] = $v;
                            unset($plugins_sel_tabs['meta'][$k]);
                        }

                return Base::view("admin::module.update", [
                    'plugins'      => $plugins_sel,
                    'plugins_tabs' => $plugins_sel_tabs,
                    'lang'         => $lang,
                    'modules'      => $modules,
                    'data'         => $data,
                    'page'         => $page,
                    'id'           => $id,
                    'plugins_lang' => $plugins_lang,
                    'right'        => $this->right,
                    'show_lang'    => !empty($modules['lang']),

                    'tabs' => [
                        'main' => 'Основные',
                        'meta' => 'Meta данные',
                        'html_insert' => 'Html вставки'
                    ],

                    'order' => $modules['plugins'],
                    'url'   => $url,
                ]);
            }
        } catch (\Exception $err) {
            return response($err->getMessage(), 500);
        }
    }
}
