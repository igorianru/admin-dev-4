@extends('admin::layouts.default')
@section('title',"Админ панель - Статистика")
@section('head')
    <link href="{{ asset('/modules/js/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/modules/js/datatables/buttons.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/modules/js/datatables/fixedHeader.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/modules/js/datatables/responsive.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/modules/js/datatables/scroller.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <style>
        .img_none img{
            display: none;
        }
    </style>
@endsection
@section('content')

    @include('admin::layouts.left-menu')
    @include('admin::layouts.top-menu')
    <div class="right_col" role="main">
        <!-- graph area -->
        <div class="col-md-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Стасистика обработки <small>Разность обработки заказов в минутах</small></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content2">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="form-group">
                                <input
                                    class="form-control"
                                    name="date_time"
                                    style="max-width: 350px"
                                    value="{{ ($_GET['start_date'] ?? '') . ' - ' . ($_GET['end_date'] ?? '') }}"
                                />
                            </div>
                        </div>
                    </div>
                    <div id="graph_area" style="width:100%; height:300px; clear: both"></div>

                    <br />

                    <div class="table-responsive">
                        <table id="table_id" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>ID заказа</th>
                                <th>Количестко товаров</th>
                                <th>Общая сумма заказа</th>
                                <th>Текущий статус заказа</th>
                                <th>Дата создания</th>
                                <th>Дата закрытия</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@section('footer')
    <!-- pace -->
    <script src="{{ asset('/modules/js/pace/pace.min.js') }}"></script>
    <!-- Datatables-->
    <script src="{{ asset('/modules/js/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('/modules/js/datatables/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('/modules/js/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('/modules/js/datatables/buttons.bootstrap.min.js') }}"></script>
    <script src="{{ asset('/modules/js/datatables/jszip.min.js') }}"></script>
    <script src="{{ asset('/modules/js/datatables/pdfmake.min.js') }}"></script>
    <script src="{{ asset('/modules/js/datatables/vfs_fonts.js') }}"></script>
    <script src="{{ asset('/modules/js/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('/modules/js/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ asset('/modules/js/datatables/dataTables.fixedHeader.min.js') }}"></script>
    <script src="{{ asset('/modules/js/datatables/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('/modules/js/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('/modules/js/datatables/responsive.bootstrap.min.js') }}"></script>
    <script src="{{ asset('/modules/js/datatables/dataTables.scroller.min.js') }}"></script>

    <script src="/modules/js/moris/morris.min.js"></script>
    <script src="/modules/js/moris/raphael-min.js"></script>

    <script>
		$("#graph_area").length && Morris.Area({
			element: "graph_area",

			data: [
//				{period: "Новый заказ", iphone: 2666, ipad: null, itouch: 2647},
//				{period: "Попал в обработку Q1", iphone: 2666, ipad: null, itouch: 2647},
				{period: "2014-10-10 20:10:25", inwork: 10, sent: 15, delivered: 15},
				{period: "2014-10-10 10:10:25", inwork: 30, sent: 15, delivered: 15},
				{period: "2014-10-10 15:10:25", inwork: 10, sent: 15, delivered: 15},
				{period: "2014-10-10 16:10:25", inwork: 15, sent: 15, delivered: 15},
				{period: "2014-10-10 20:10:25", inwork: 14, sent: 15, delivered: 15},
				{period: "2014-10-10 17:10:25", inwork: 15, sent: 15, delivered: 15},
				{period: "2014-10-10 20:10:25", inwork: 15, sent: 15, delivered: 15},
				{period: "2014-10-10 15:10:25", inwork: 15, sent: 15, delivered: 15},
				{period: "2014-10-10 18:10:25", inwork: 15, sent: 15, delivered: 15}
			],

			xkey: "period",
			ykeys: ["inwork", "sent", "delivered"],
			lineColors: ["#26B99A", "#34495E", "#ACADAC", "#3498DB"],
			labels: ["в обработке", "Отправлено", "Доставлено"],
			pointSize: 2,
			hideHover: "auto",
			resize: !0
		});

		$('[name=date_time]').daterangepicker({
			format: 'YYYY-MM-DD',
			language: 'ru',
//			timePicker: true,
			calender_style: "picker_4",
			timePickerIncrement: 10,
		}).on('apply.daterangepicker', function (ev, picker) {
			window.location.href = '/admin/index/statistics?start_date=' + picker.endDate.format('YYYY-MM-DD') +
                '&end_date=' + picker.endDate.format('YYYY-MM-DD');
		});

		$('#table_id').DataTable({
//			"bServerSide": true,
			"iDisplayLength": 10,

			columns: [
				{data: 'id'},
				{data: 'count_products'},
				{data: 'total_amount'},
				{data: 'status'},
				{data: 'created_at'},
				{data: 'closed_at'}
			]
		});
    </script>
@endsection
@stop