@foreach($carts as $val)
    <div class="col-md-2 product-{{ $val['id'] }}">
        <div class="shop-cart-big">
            <div>
                <div class="t thumbnail">
                    <a href="/shop/product/{{ $val['id'] }}" target="_blank">
                        @if($val['file'])
                            @if($val['crop'])
                                <img src="/images/files/small/{{ $val['crop'] }}" />
                            @else
                                <img src="/images/files/small/{{ $val['file'] }}" />
                            @endif
                        @else
                            <img src="/images/files/small/no_img.png" class="blur1" style="width: 100%" />
                        @endif
                    </a>
                </div>
                <div class="b">
                    <div class="z"><h4>{{ $val['name'] }}</h4></div>
                    <div class="m">
                        <span class="weight"><span>{{ $val['weights'] / 1000 }}</span>
                            {{ $type_measure[$val['type_measure']] }}
                        </span>

                        <div class="p">
                            <div class="col-md-6 text-left">
                                <div class="row price-money">
                                    <?php
                                    $price = ($val['price_money'] - ($val['price_money']/100 * $val['discount'])) / 1000
                                        * $val['weights']
                                    ?>
                                    <span data-price-kilogram="{{ $price }}">
                                        {{ $price }} <i class="glyphicon glyphicon-ruble"></i>
                                    </span>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endforeach

<br class="clear"/>
<h3 style="width: 100%">История статусов</h3>

<ul>
    @foreach($statuses as $val)
        <li>{{ $status_app[$val['status']] . ' ' . $val['created_at'] }}</li>
    @endforeach
</ul>
