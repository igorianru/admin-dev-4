<!-- top navigation -->
<div class="top_nav">

    <div class="nav_menu">
        <nav class="" role="navigation">
            <div class="nav toggle">
                <a id="menu_toggle" style="cursor: pointer"><i class="fa fa-bars"></i></a>
            </div>
            <span style="line-height: 60px; float: left;">Админ панель {{ $version }}</span>
            <ul class="nav navbar-nav navbar-left">
                <li>
                    <a class="" style="color: #da2726!important;" href="/admin/index/applications?pl[status_app]=0">
                        Новые <b class="badge">{{ $status_app_0['r'] }}</b>
                    </a>
                </li>
                <li>
                    <a class="" style="color: #24B550!important;" href="/admin/index/applications?pl[status_app]=1">
                        В обработке <b class="badge">{{ $status_app_1['r'] }}</b>
                    </a>
                </li>
                <li>
                    <a class="" style="color: #220525!important;" href="/admin/index/applications?pl[status_app]=2">
                        Отправлены <b class="badge">{{ $status_app_2['r'] }}</b>
                    </a>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="/" target="_blank">
                        <i class="fa fa-external-link"></i>
                        Открыть сайт
                    </a>
                </li>
                <li class="">
                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown"
                       aria-expanded="false">
                        <img src="/modules/images/user.png" alt="">
                        @if (!empty(\App\Classes\Base::$user))
                            {{ \App\Classes\Base::$user->name }}
                        @else
                            {{ 'Администратор' }}
                        @endif
                        <span class=" fa fa-angle-down"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                        <li>
                            <a href="javascript:;"> Профиль</a>
                        </li>
                        <li>
                            <a href="javascript:;">Помощь</a>
                        </li>
                        <li>
                            <a href="/admin/logout">
                                <i class="fa fa-sign-out pull-right"></i>
                                Выйти
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
    </div>

</div>
<!-- /top navigation -->