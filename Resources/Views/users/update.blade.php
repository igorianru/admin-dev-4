@extends('admin::layouts.default')
@section('title',"Админ панель SMV 4.0DEV")
@section('content')

    @include('admin::layouts.left-menu')
    @include('admin::layouts.top-menu')
    <div class="right_col" role="main">
        <br />

        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>
                            Пользователи
                            @if(empty($data))
                                <small>добавление</small>
                            @else
                                <small>редактирование</small>
                            @endif
                        </h2>
                        <hr class="clear"/>

                        <form method="post" class="form-modules form-horizontal form-label-left">
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Имя</label>

                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input
                                        type="text"
                                        name="pl[name]"
                                        value="{{ $data->name or '' }}"
                                        id="inputName"
                                        class="form-control"
                                        placeholder="Имя"
                                    >
                                </div>

                                <br class="clear"/>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Описание</label>

                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <textarea
                                        class="form-control"
                                        id="textareaText"
                                        name="pl[text]"
                                        placeholder="Описание"
                                        rows="3"
                                    >{{ $data->text or '' }}</textarea>
                                </div>

                                <br class="clear"/>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Статус</label>

                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select name="pl[active]" id="selectActive" class="form-control select2">
                                        <option value="1" {!! (isset($data->active) ? $data->active : '') == 1 ? 'selected' : '' !!}>Активен</option>
                                        <option value="0" {!! (isset($data->active) ? $data->active : '') == 0 ? 'selected' : '' !!}>Не активен</option>
                                    </select>
                                </div>

                                <br class="clear"/>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">E-mail</label>

                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input
                                        type="text"
                                        name="pl[email]"
                                        value="{{ $data->email or '' }}"
                                        id="inputEmail"
                                        class="form-control"
                                        placeholder="E-mail"
                                    >
                                </div>

                                <br class="clear"/>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Текущий пароль</label>

                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input
                                        type="text"
                                        value="{{ $data->save_password or '' }}"
                                        class="form-control"
                                        id="exampleInputEmail"
                                        placeholder="Пароль"
                                        disabled
                                    />
                                </div>

                                <br class="clear"/>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Новый пароль</label>

                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input
                                        type="text"
                                        name="pl[password]"
                                        autocomplete="false"
                                        value=""
                                        class="form-control"
                                        id="exampleInputEmail"
                                        placeholder="Новый пароль"
                                    >
                                </div>

                                <br class="clear"/>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Тип прав</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select name="pl[usertype]" id="selectRight" class="form-control select2">
                                        <option value="user" {!! (isset($data->usertype) ? $data->usertype : '') == 'user' ? 'selected' : '' !!}>Смешанные права</option>
                                        <option value="admin" {!! (isset($data->usertype) ? $data->usertype : '') == 'admin' ? 'selected' : '' !!}>Полные права</option>
                                    </select>

                                </div>
                                <br class="clear"/>
                            </div>

                            <div class="module-right">
                                @foreach($modules as $val)
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                            {{ $val['name_module'] }}
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="hidden" value="{{ $val['id'] }}" name="id_menu[{{ $val['id'] }}]" />

                                            <label>

                                                <input type="checkbox" class="flat" value="1" name="r[{{ $val['id'] }}]" {!! (isset($val['r']) ? $val['r'] : '') == 1 ? 'checked' : '' !!}/>
                                                Просмотр
                                            </label>

                                            <label>
                                                <input type="checkbox" class="flat" value="1" name="x[{{ $val['id'] }}]" {!! (isset($val['x']) ? $val['x'] : '') == 1 ? 'checked' : '' !!}/>
                                                Изменение
                                            </label>

                                            <label>
                                                <input type="checkbox" class="flat" value="1" name="w[{{ $val['id'] }}]" {!! (isset($val['w']) ? $val['w'] : '') == 1 ? 'checked' : '' !!}/>
                                                Создание
                                            </label>

                                            <label>
                                                <input type="checkbox" class="flat" value="1" name="d[{{ $val['id'] }}]" {!! (isset($val['d']) ? $val['d'] : '') == 1 ? 'checked' : '' !!}/>
                                                Удалеине
                                            </label>

                                            <hr style="margin: 0" />
                                        </div>
                                        <br class="clear"/>
                                    </div>
                                @endforeach
                            </div>

                            <div class="loader"></div>
                            <button class="btn btn-success" type="submit">Сохранить</button>
                            <button class="btn btn-primary" formaction="/admin/update/users/{{ $id }}/1" type="submit">Применить</button>
                            <button class="btn btn-default" formaction="/admin/index/users" type="submit">Отменить</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="{{ asset('/modules/js/modules.js') }}"></script>
    <script>
		$(document).ready(function(){
			modules.initialize({});

			$('#selectRight').on('change', function (e) {
				var display = this.value === 'admin' ? 'none' : 'block';

				$('.module-right').css({display: display})
			});

			if($('#selectRight').val() === 'admin') $('.module-right').css({display: 'none'})
		});
    </script>
    </div>
@stop