@extends('admin::layouts.default')
@section('title',"Авторизация")
@section('footer')

@endsection
@section('content')

{{--    @include('/admin/layouts/left-menu')--}}
{{--    @include('/admin/layouts/top-menu')--}}
    <div id="wrapper">
        <div id="login" class="animate form"  style="background: #fff; padding: 10px">
            <section class="login_content">
                <form method="post">
                    <h1>Авторизация</h1>
                    <div>
                        <input type="text" name="email" class="form-control" placeholder="Логин" required="" />
                    </div>
                    <div>
                        <input type="password" name="password" class="form-control" placeholder="Пароль" required="" />
                    </div>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div>
                        <button type="submit" class="btn btn-default submit" href="/admin/login">Войти</button>
                        {{--<a class="reset_pass" href="#">Lost your password?</a>--}}
                    </div>
                    <div class="clearfix"></div>
                    <div class="separator">
                        <div class="clearfix"></div>
                        <br />
                        <div>
                            <h1><i class="fa fa-paw" style="font-size: 26px;"></i> SMV 4.0DEV</h1>
                        </div>
                    </div>
                </form>
                <!-- form -->
            </section>
            <!-- content -->
        </div>
        <div style="clear: both; width: 100%"></div>
    </div>
@stop