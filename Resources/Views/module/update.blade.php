@extends('admin::layouts.default')

@section('title', isset($data['name'])
    ? 'Редактирование - ' . json_decode($data['name'], true)[App::getLocale()] ?? $data['name']
    : 'Добавление'
)

@section('content')

    @include('admin::layouts.left-menu')
    @include('admin::layouts.top-menu')
    <div class="right_col" role="main">
        <br />

        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>
                            {{ $modules['name_module'] }}
                            @if(empty($data))
                                <small>добавление</small>
                            @else
                                <small>редактирование</small>
                            @endif
                        </h2>
                        <hr class="clear"/>

                        <form method="post" class="form-modules form-horizontal form-label-left">
                            <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                    @foreach($plugins_tabs as $key => $v)

                                        <li role="presentation" class="{!! $key == 'main' ? 'active' : '' !!}">
                                            <a
                                                href="#url-{{ $key }}-tab"
                                                id="{{ $key }}-tab"
                                                role="tab"
                                                data-toggle="tab"
                                                aria-expanded="{!! $key == 'main' ? 'true' : 'false' !!}"
                                            >
                                                {{ $tabs[$key] }}
                                            </a>
                                        </li>
                                    @endforeach

                                    @if($show_lang)
                                        @foreach($lang as $key => $val)
                                            <li role="presentation" class="">
                                                <a
                                                    href="#url-{{ $key }}-tab"
                                                    id="{{ $key }}-tab"
                                                    role="tab"
                                                    data-toggle="tab"
                                                    aria-expanded="false"
                                                >
                                                    {{ $val['name'] }}
                                                </a>
                                            </li>
                                        @endforeach
                                    @endif
                                </ul>

                                <div id="myTabContent" class="tab-content">
                                    @foreach($plugins_tabs as $key => $v)
                                        <div
                                            role="tabpanel"
                                            class="tab-pane fade {!! $key == 'main' ? 'active in' : '' !!}"
                                            id="url-{{ $key }}-tab"
                                            aria-labelledby="{{ $key }}-tab"
                                        >
                                            @foreach($v as $key => $val)
                                                {!! $val['html_top'] !!}
                                                {!! str_replace('--options--', '', $val['html']) !!}
                                                {!! $val['html_bottom'] !!}

                                                @if($val['name'] == 'album')
                                                    <script>
														$.ajax({
															type: "post",
															url: "/admin/file/get_loader_img",
															data: {
																name_table : '{{ $page }}',
																id_album   : '{{ $id }}'
															},
															cache: false,
															dataType: "html",
															success: function (data) {
																$('#album').html(data);
															}
														})
                                                    </script>
                                                @endif

												@if($val['name'] == 'ordering')
													<script>
														$.ajax({
															type: "post",
															url: "/admin/component/get_loader_ordering",
															data: {
																name_table : '{{ $page }}',
																id         : '{{ $id }}'
															},
															cache: false,
															dataType: "html",
															success: function (data) {
																$('#ordering').html(data);
															}
														})
													</script>
												@endif
                                            @endforeach
                                        </div>
                                    @endforeach

                                    @if($show_lang)
                                        @foreach($lang as $key => $val)
                                            <div
                                                role="tabpanel"
                                                class="tab-pane fade"
                                                id="url-{{ $key }}-tab"
                                                aria-labelledby="{{ $key }}-tab"
                                            >
                                                <div class="wrapper wrapper-content animated fadeIn">
                                                    @foreach($plugins_lang as $k => $v)
                                                        {!! $v['html_top'] !!}

                                                        @if($v['name'] == 'location_text')
                                                            {!! str_replace('--options--', '[' . strtolower($val['name']) . ']', $v['html']) !!}
                                                        @else
                                                            {!! str_replace('--options--', '[' . strtolower($val['name']) . ']', $v['html']) !!}
                                                        @endif

                                                        {!! $v['html_bottom'] !!}

                                                        @if($v['name'] == 'location_text')
                                                            <script>
																$.ajax({
																	type: "post",
																	url: "/admin/modules/show_loader",
																	data: {
																		name_table : '{{ $table }}',
																		id_album   : '{{ $id }}',
																		view       : 'locationText',
																		lang       : '{{ $val['name'] }}'
																	},
																	cache: false,
																	dataType: "html",
																	success: function (data) {
																		$('#location_text{{ $val['name'] }}').html(data);
																	}
																})
                                                            </script>
                                                        @endif
                                                    @endforeach
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>

                            <div class="text-right">
                                <div class="loader"></div>
                                <button class="btn btn-success" type="submit">Сохранить</button>
                                <button class="btn btn-primary" formaction="/admin/update/{{ $page }}/{{ $id }}/1" type="submit">Применить</button>
                                <button class="btn btn-default" formaction="/admin/index/{{ $page . $url }}" type="submit">Отменить</button>
                            </div>
                        </form>

                        @if(!empty($data))
                            <script>
								$(document).ready(function () {
									function ucfirst( str ) {
										var f = str.charAt(0).toUpperCase();

										return f + str.substr(1, str.length-1);
									}

									var column, text, typeField;
									var body = {!! json_encode($data) !!};
									var plugins = {!! json_encode($plugins) !!};
									var pluginsLang = {!! json_encode($plugins_lang) !!};

									_.map(pluginsLang, function(v, k) {
										text = _.unescape(body[v.name]);

										try {
											text = JSON.parse(text);
											if(text) {
												$('[name="pl[' + v.name + '][ru]"]').val(_.unescape(text.ru));
												$('[name="pl[' + v.name + '][en]"]').val(_.unescape(text.en));
											}
										} catch (err) {
											// обработка ошибки
											// вставляем текст как он есть в поле которое в последсвии будет преобразовано в
											// json

											$('[name="pl[' + v.name + '][ru]"]').val(text);
											$('[name="pl[' + v.name + '][en]"]').val(text);
										}
									});

									$('.select2').select2("destroy");
									_.map(plugins, function(v, k) {
										text = body[k];

										if((v.plugins || {}).typeField === 'select' && text) {
											$('#select' + ucfirst(k)).val(text ? text : 0).trigger("change");
										} else {
											$('[name="pl[' + k + ']"]').val(_.unescape(text));
										}
									});

									$(".select2").select2();
								})
                            </script>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="{{ asset('/modules/js/modules.js') }}"></script>
    <script type="text/javascript" src="/js/lodash.min.js"></script>
    <script>
		$(document).ready(function(){
			modules.initialize({});
		});
    </script>

    @if($page == 'conference')
        <script>
			$(document).ready(function(){
				(function () {
					function dtabnewmap() {
						ymaps.ready(function () {
							var myMap = new ymaps.Map("map-l", {
									center: [48.7110, 44.5264],
									zoom: 12,
									controls: ['zoomControl']
								}),
								// Создаем экземпляр класса ymaps.control.SearchControl
								mySearchControl = new ymaps.control.SearchControl({
									options: {
										noPlacemark: true
									}
								}),
								// Результаты поиска будем помещать в коллекцию.
								mySearchResults = new ymaps.GeoObjectCollection(null, {
									draggable: true,
									hasHint: false
								});

							myMap.controls.add(mySearchControl);
							myMap.geoObjects.add(mySearchResults);

							myMap.events.add('balloonopen', function (e) {
								var thisPlacemark = e.get('target');
								var coords = thisPlacemark.geometry.getCoordinates();

								ymaps.geocode(coords, {
									results: 1
								}).then(function (res) {
									var newContent = res.geoObjects.get(0) ?
										res.geoObjects.get(0).properties.get('name') :
										'Не удалось определить адрес.';

									// Задаем новое содержимое балуна в соответствующее свойство метки.
									thisPlacemark.properties.set('balloonContentBody', newContent);
								});

							});

							mySearchResults.events.add('dragstart', function (e) {
								var thisPlacemark = e.get('target');
								thisPlacemark.options.set('preset', 'islands#blueIcon');
							});

							mySearchResults.events.add('dragend', function (e) {
								var thisPlacemark = e.get('target');
								thisPlacemark.options.set('preset', 'islands#redIcon');
								var coords = thisPlacemark.geometry.getCoordinates();

								$("#inputCoordinates").val(coords[0].toPrecision(10) + ", " + coords[1].toPrecision(10));

								ymaps.geocode(coords, {
									results: 1
								}).then(function (res) {
									var newContent = res.geoObjects.get(0) ?
										res.geoObjects.get(0).properties.get('name') :
										'Не удалось определить адрес.';

									$("#inputLocation").val(res.geoObjects.get(0).properties.get('text'));
									// Задаем новое содержимое балуна в соответствующее свойство метки.
									thisPlacemark.properties.set('balloonContentBody', newContent);
								});
							});

							mySearchControl.events.add('resultselect', function (e) {
								var index = e.get('index');

								mySearchControl.getResult(index).then(function (res) {
									mySearchResults.add(res);
									var coords = res.geometry.getCoordinates();

									ymaps.geocode(coords, {
										results: 1
									}).then(function (res) {
										var newContent = res.geoObjects.get(0)
											? res.geoObjects.get(0).properties.get('name')
											: 'Не удалось определить адрес.';

										$("#inputLocation").val(res.geoObjects.get(0).properties.get('text'));

										// Задаем новое содержимое балуна в соответствующее свойство метки.
										myMap.balloon.open(coords, newContent);
									});

									$("#inputCoordinates").val(coords[0].toPrecision(10) + ", " + coords[1].toPrecision(10));
								});
							}).add('submit', function () {
								mySearchResults.removeAll();
							})
						});
					}

					dtabnewmap();
				})();
			});
        </script>
        <script src="//api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
    @endif
@stop