<script type="text/javascript" src="{{ asset('/modules/js/jquery-fileTree/enhance.js') }}"></script>
<script src="{{ asset('/modules/js/jquery-fileTree/jQuery.tree.js') }}" type="text/javascript"></script>
<link href="{{ asset('/modules/js/jquery-fileTree/enhanced.css') }}" rel="stylesheet" type="text/css" media="screen" />

<div>
<style>
    .inp_edit_u {
        display: block;
        float: left;
        margin-left: 28px;
        margin-top: 0px;
        padding-left: 0px;
    }
</style>

<?php

function view_cat($arr,$cat = 0, $i = 0, $rights)
{
    if(empty($arr[$cat])) {
        return;
    }

    if($cat != 0) {
        $r = 'display:';
    } else {
        $r = '';
    }

    if(isset($arr[$cat][$i]['cat'])) {
        $f = $arr[$cat][$i]['cat'];
    } else {
        $f = '';
    }

    if($i == 0 ) {
        $cj = 'class="files menu_tree"';
    } else {
        $cj = '';
    }
    echo '<ul '.$cj.' >';

    //перебираем в цикле массив и выводим на экран
    for($i = 0; $i < count($arr[$cat]);$i++)
    {
        $type_status = array(0=>'Не оторбажается',1=>'Отображается');
        $class_status = array(0=>'close',1=>'open');
//            $l = '/admin/menu_edit/'.$arr[$cat][$i]['id'].'';
        $l = '#';

        $name = json_decode($arr[$cat][$i]['name'], true)[App::getLocale()] ?? $arr[$cat][$i]['name'];

        echo '
        <li class="rowID-'. $arr[$cat][$i]['id'] .'" id="rowID'. $arr[$cat][$i]['id'] .'">
            <div class="inp_edit_u inp_edit_'.$arr[$cat][$i]['id'].'">
                <input type="radio" name="id_m" autocomplete="off" class="inp_edit flat" title="'. $name .'"
             value="'.$arr[$cat][$i]['id'].'" id="'.$arr[$cat][$i]['id'].'"/>
            </div>
            <a href="'.$l.'" >&nbsp;&nbsp;' . $name .'</a>';
        ?>

            <?php
        //рекурсия - проверяем нет ли дочерних категорий
        view_cat($arr,$arr[$cat][$i]['id'], $i, $rights);

        echo '</li>';
    }
    echo '</ul>';
}

    view_cat($menu,0, 0, 1)
    //view_cat($menu,0, 0, $rights)
    ?><br />

</div>

<script>
    $('.menu_tree').tree({
        expanded: 'li:first'
    });

    $(document).ready(function() {
        clM('.rowID-{{ $get_id }}');

        function clM(c) {
            var cl = $(c).parent().parent('li').attr('class');

            if(cl != undefined) {
                $(c).parent().parents('li').children('a').click();
                cl = $(c).parent().parent('li').attr('class');

                clM(cl);
            }
        }

        setTimeout(function() {
            $('.rowID-{{ $get_id }} > a').click();
            $('.rowID-{{ $get_id }} > div > input').click();

            window.location.href = window.location.href.split('#')[0] + '#{{ $get_id }}';
        }, 1000);
    });
</script>
