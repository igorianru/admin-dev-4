@extends('admin::layouts.default')
@section('title',"Админ панель SMV 4.0DEV")
@section('content')

    @include('admin::layouts.left-menu')
    @include('admin::layouts.top-menu')
    <div class="right_col" role="main">
        <br />

        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>{{ $modules['name_module'] }}</h2>
                        <div class="nav navbar-right panel_toolbox">
                            @if($right['x'])
                                <a href="/admin/update/{{ $modules['link_module'] }}" class="btn btn-primary">Добавить</a>
                            @endif
                        </div>
                        <div class="clearfix"></div>
                        <hr class="clear" />

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon">Выберите действие: </span>

                                <select onchange="$.adm.makeD(this.value)" id="select1" class="form-control select" autocomplete="off">
                                    <option value="0" id="defo">Выберите действие</option>
                                    <option value="edit">Редактировать</option>
                                    <option value="delete">Удалить</option>
                                    <option value="copy">Копировать</option>
                                </select>
                            </div>

                            <div class="error"></div>
                            <div class="sass"></div>
                            <input type="hidden" name="id_mt" class="id_mt"/>
                        </div>

                        @if($modules['views_module'] == 'wood')
                            @include('admin::module/index_wood')
                        @else
                            @include('admin::module/index_table')
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="{{ asset('/modules/js/adm.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/modules/js/modules.js') }}"></script>
    <script>
        $(document).ready(function(){
            modules.initialize({

            });

            $.adm.initialize({
                pram: [
                    ['elementsLoad', true],
                    ['link_module', '{{ $modules['link_module'] }}']
                ]
            });
        });
    </script>
@stop