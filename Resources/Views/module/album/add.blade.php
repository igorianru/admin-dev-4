<!-- image cropping -->
<script src="{{ asset('modules/js/cropping/cropper.min.js') }}"></script>
<script src="{{ asset('modules/js/cropping/main2.js') }}"></script>
<script src="{{ asset('/modules/js/upl_mul/jquery.uploadifive.min.js') }}" type="text/javascript"></script>

<style>
    .im_sel
    {
        display: none;
        margin: 0 ;
        height:29px;
    }
    .im_sel_a
    {
        cursor: pointer;
        /*color: #7d73a2;*/
        font-size: 15px;
    }
    .im_sel_t
    {

        background: #fafafa;
        border: solid 1px #f6f6f6;
        border-radius: 2px;
        width: 100%;
        /*   text-align: center;*/
    }
    .imag_bat
    {
        display: block;
        margin: 10px auto;
        padding: 10px 50px;
    }
    /*
Uploadify
Copyright (c) 2012 Reactive Apps, Ronnie Garcia
Released under the MIT License <http://www.opensource.org/licenses/mit-license.php>
*/

    .uploadify {
        position: relative;
        margin-bottom: 1em;
    }
    .uploadify-button {
        background-color: #505050;
        background-image: linear-gradient(bottom, #505050 0%, #707070 100%);
        background-image: -o-linear-gradient(bottom, #505050 0%, #707070 100%);
        background-image: -moz-linear-gradient(bottom, #505050 0%, #707070 100%);
        background-image: -webkit-linear-gradient(bottom, #505050 0%, #707070 100%);
        background-image: -ms-linear-gradient(bottom, #505050 0%, #707070 100%);
        background-image: -webkit-gradient(
                linear,
                left bottom,
                left top,
                color-stop(0, #505050),
                color-stop(1, #707070)
        );
        background-position: center top;
        background-repeat: no-repeat;
        -webkit-border-radius: 30px;
        -moz-border-radius: 30px;
        border-radius: 30px;
        border: 2px solid #808080;
        color: #FFF;
        font: bold 12px Arial, Helvetica, sans-serif;
        text-align: center;
        text-shadow: 0 -1px 0 rgba(0,0,0,0.25);
        width: 100%;
    }
    .uploadify:hover .uploadify-button {
        background-color: #606060;
        background-image: linear-gradient(top, #606060 0%, #808080 100%);
        background-image: -o-linear-gradient(top, #606060 0%, #808080 100%);
        background-image: -moz-linear-gradient(top, #606060 0%, #808080 100%);
        background-image: -webkit-linear-gradient(top, #606060 0%, #808080 100%);
        background-image: -ms-linear-gradient(top, #606060 0%, #808080 100%);
        background-image: -webkit-gradient(
                linear,
                left bottom,
                left top,
                color-stop(0, #606060),
                color-stop(1, #808080)
        );
        background-position: center bottom;
    }
    .uploadify-button.disabled {
        background-color: #D0D0D0;
        color: #808080;
    }
    .uploadify-queue {
        margin-bottom: 1em;
    }
    .uploadify-queue-item {
        background-color: #F5F5F5;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        font: 11px Verdana, Geneva, sans-serif;
        margin-top: 5px;
        max-width: 350px;
        padding: 10px;
    }
    .uploadify-error {
        background-color: #FDE5DD !important;
    }
    .uploadify-queue-item .cancel a {
        background: url('{{ asset('modules/js/upl_mul/uploadify-cancel.png') }}') 0 0 no-repeat;
        float: right;
        height:	16px;
        text-indent: -9999px;
        width: 16px;
    }
    .uploadify-queue-item.completed {
        background-color: #E5E5E5;
    }
    .uploadify-progress {
        background-color: #E5E5E5;
        margin-top: 10px;
        width: 100%;
    }
    .uploadify-progress-bar {
        background-color: #0099FF;
        height: 3px;
        width: 1px;
    }

    .queue {
        max-width: 350px;
    }
    .close_n {
        /*position: absolute;*/
        right: -29px;
        top: 0px;
        float: left;
    }

    .close_n > button {
        padding: 0 5px
    }

    .queue {
        margin-bottom: 5px;
    }
    .uploadifive-queue-item.error {
        background: rgb(249, 212, 212) none repeat scroll 0% 0%;
    }
    .uploadifive-queue-item.success {
        background: rgb(211, 255, 210) none repeat scroll 0% 0%;
    }
    .uploadifive-queue-item {
        clear: both;
        margin-bottom: 2px;
        padding: 2px;
        border: 1px solid rgb(210, 233, 255);
    }
    .uploadifive-queue-item > div {
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
    }
</style>

<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne" style="padding:0">
            <h4 class="panel-title">
                <div  style="padding: 10px 15px" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    Альбом
                </div>
            </h4>
        </div>
        <div id="collapseOne" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">

                @if($id_album != '0')
                    @php($id_page = 1)
                    <form style="margin-bottom: 5px">

                        <input id="file_upload" name="file_upload" type="file" multiple="multiple">
                        <!--        <a style="position: relative; top: 8px;" href="javascript:$('#file_upload').uploadifive('upload')">Upload Files</a>-->
                    </form>

                    <form name="" id="imag_main_form">
                        <div id="queue" class="alert queue"></div>
                        <div class="response_suss" id="response_suss">
                            @foreach($files as $v)
                                <div class="col-md-4 rowID-{{ $v->id }}">
                                    <div class="thumbnail">
                                        <div class="image view view-first">
                                            @if($v->crop)
                                                <img src="/images/files/small/{{  $v->crop }}" style="width: 100%; display: block;"/>
                                            @else
                                                <img src="/images/files/small/{{ $v->file }}" style="width: 100%; display: block;"/>
                                            @endif
                                        </div>
                                        <div class="caption" style="padding-bottom: 0">
                                            <div class="tools tools-bottom" style="text-align: center">
                                                <a
                                                        href="javascript:void(0)"
                                                        class="btn"
                                                        title="Редактировать"
                                                        onclick="editImg({{ $v->id }})"
                                                >
                                                    <i class="fa fa-pencil"></i>
                                                </a>

                                                <a
                                                        href="javascript:void(0)"
                                                        class="btn"
                                                        title="Обрезать"
                                                        onclick="cropImg({{ $v->id }})"
                                                >
                                                    <i class="fa fa-crop"></i>
                                                </a>

                                                <a
                                                        href="javascript:void(0)"
                                                        class="btn"
                                                        title="Удалить"
                                                        onclick="$.adm.rowDelete('{{ $v->id }}', '\'files\'')"
                                                >
                                                    <i class="fa fa-times"></i>
                                                </a>

                                                <a
                                                        href="javascript:void(0)"
                                                        class="btn"
                                                        title="Сделать главной"
                                                        onclick="toMain({{ $v->id }})"
                                                >
                                                    <i class="toMain toMain{{ $v->id }} glyphicon @if($v->main) glyphicon-check @else glyphicon-unchecked @endif"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                            <input type="hidden" id="vt_i" name="fo[o][id_img]" value="0"/>
                            <input type="hidden" id="vt_i" name="fo[id_album]" value="{{ $id_page }}"/>
                        </div>
                    </form>

                    @php($timestamp = time())
                    <script type="text/javascript">
                        var typeFile = [
                            'image/jpg',
                            'image/gif',
                            'image/GIF',
                            'image/png',
                            'image/jpeg',
                            'image/JPG',
                            'image/PNG',
                            'image/JPEG'
                        ];

                        $(function() {
                            $('#file_upload').uploadifive({
                                'formData'       : {
                                    'timestamp'  : '{{ $timestamp }}',
                                    'token'      : '{{ md5('file_upload' . $timestamp) }}',
                                    'id_album'   : '{{ $id_album }}',
                                    'name_table' : '{{ $name_table }}'
                                },

                                'debug'        : true,
                                'queueID'      : 'queue',
                                'buttonText'   : 'Выбрать изображения',
                                'buttonClass'  : 'btn btn-primary imag_bat',
                                'width'        : 350,
                                'height'       : 40,
                                'lineHeight'   :  '20px',
                                'fileType'     :  typeFile,
                                'fileDesc'     : 'All supported files types (.pdf, .jpeg)',
                                'uploadScript' : '/admin/files/upload_img?name_table={{ $name_table }}&id_album={{ $id_album }}&timestamp={{ $timestamp }}&token={{ md5('file_upload' . $timestamp) }}',
                                'onProgress'   : 'total',
                                'fileSizeLimit': '10048KB',

                                'onUploadComplete' : function(file, data)
                                {
                                    var p = jQuery.parseJSON(data);
                                    var ds = JSON.parse(data);

                                    var img = '<div class="col-md-4 rowID-' + ds['id'] + '">' +
                                        '<div class="thumbnail">' +
                                        '<div class="image view view-first">' +
                                        '<img src="/images/files/small/' + ds['name'] + '" style="width: 100%; display: block;"/>' +
                                        '</div>' +
                                        '<div class="caption" style="padding-bottom: 0">' +
                                        '<div class="tools tools-bottom" style="text-align: center">' +
                                        '<a href="javascript:void(0)" onclick="editImg(' + ds['id'] + ')" class="btn"><i class="fa fa-pencil"></i></a>' +
                                        '<a href="javascript:void(0)" onclick="cropImg(' + ds['id'] + ')" class="btn"><i class="fa fa-crop"></i></a>' +
                                        '<a href="javascript:void(0)" onclick="rowDelete(' + ds['id'] + ', \'files\')" class="btn"><i class="fa fa-times"></i></a>' +
                                        '<a href="javascript:void(0)" class="btn" onclick="toMain(' + ds['id'] + ')">';
                                    if(ds['main'] == 1) {
                                        img += '<i class="toMain toMain' + ds['id'] + ' glyphicon glyphicon-check"></i>';
                                    } else {
                                        img += '<i class="toMain toMain' + ds['id'] + ' glyphicon glyphicon-unchecked"></i>';
                                    }

                                    img += '</a>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>';

                                    $('#response_suss').append(img);
                                    $("#vt_a").show(100);
                                }
                            });
                        });

                        function cropImg(id) {
                            $.ajax
                            ({
                                type: "POST",
                                url: "/admin/files/get_crop",
                                dataType: 'html',
                                data: {
                                    id: id
                                },
                                cache: false,
                                success: function (html)
                                {
                                    $('.alb-modal > div > div > div > .modal-title').html('Редактирование изображение');
                                    $('.modal-lgAl').css({width: '100%'});
                                    $('#alb-modal').modal('show');
                                    $('.bodyModal').html(html);

                                    setTimeout(function () {
                                        $('.btnsav').attr('data-id', ''+id+'')
                                    }, 300);
                                }
                            });
                        }

                        function editImg(id) {
                            $.ajax
                            ({
                                type: "POST",
                                url: "/admin/files/get_edit",
                                dataType: 'html',
                                data: {
                                    id: id
                                },
                                cache: false,
                                success: function (html)
                                {
                                    $('.alb-modal > div > div > div > .modal-title').html('Редактировать описание');
                                    $('.modal-lgAl').css({width: '600px'});
                                    $('#alb-modal').modal('show');
                                    $('.bodyModal').html(html);

                                    setTimeout(function () {
                                        $('.btnsav').attr('data-id', ''+id+'')
                                    }, 300);
                                }
                            });
                        }

                        function sendEditImg(id) {
                            var name, text, order;

                            name = $('[name="name_img_edit"]').val();
                            text = $('[name="text_img_edit"]').val();
                            order = $('[name="order_img_edit"]').val();

                            $.ajax
                            ({
                                type: "POST",
                                url: "/admin/files/set_edit",
                                dataType: 'json',

                                data: {
                                    id: id,
                                    name: name,
                                    text: text,
                                    order: order,
                                    save: true
                                },

                                cache: false,
                                success: function (data) {
                                    $('#alb-modal').modal('hide');
                                }
                            });
                        }

                        function toMain(id) {
                            $.ajax
                            ({
                                type: "POST",
                                url: "/admin/files/to_main",
                                dataType: 'json',
                                data: {
                                    id: id
                                },
                                cache: false,
                                success: function (html)
                                {
                                    if(html.result == 'ok') {
                                        $('.toMain').removeClass('glyphicon-check');
                                        $('.toMain').addClass('glyphicon-unchecked');

                                        $('.toMain' + id).addClass('glyphicon-check');
                                        $('.toMain' + id).removeClass('glyphicon-unchecked');
                                    }
                                }
                            });
                        }

                        //                    // отправка описания после загрузки фоток
                        //                    function save_img(id)
                        //                    {
                        //                        $.ajax
                        //                        ({
                        //                            type: "POST",
                        //                            url: "/admin/files/save_img",
                        //                            dataType: 'JSON',
                        //                            data: {
                        //                                id: id,
                        //                                file: file
                        //                            },
                        //                            cache: false,
                        //                            success: function (html)
                        //                            {
                        //                                $('#alb-modal').modal('hide');
                        //                            }
                        //                        });
                        //                    }
                    </script>






                    <br/>

                    {{--<div id="vt_a" style="display: none">--}}
                    {{--<a class="btn btn-primary vt_a" onclick="save_img()">Сохранить</a>--}}
                    {{--</div>--}}


                    <span id="status"></span>
                    <ul id="files"></ul>

                    <div id="form_ij" style="width: 500px; margin: 0 auto;">
                        <div id="form_ij_img" style="margin-bottom: 10px;text-align: center"></div>
                        <!--/ получившийся рисунок /-->
                        <div id="form_ij_for"></div>
                        <!--/ форма для комментов /-->
                    </div>

                    <div class="row" id="row_crop">
                        <div class="span12">
                            <div class="jc-demo-box_e">
                                <div class="jc-demo-box">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="error_s"></div>
                    <div class="error_ok"></div>
                    <div class="result_img" id="result_img"></div>
                    <div class="crop_img" id="crop_img"></div>

                    <div class="img"></div>

                @else
                    <button class="btn btn-primary" formaction="/admin/update/{{ $name_table }}/{{ $id_album }}/1" type="submit">Включить альбом</button>
                @endif
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" id="alb-modal" role="dialog" >
    <div class="modal-dialog modal-lg modal-lgAl">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="bodyModal"> </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->