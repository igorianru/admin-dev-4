<div class="modal-body">
    <div class="panel-body">
        <input type="hidden" name="_token" value="yNlTQCa2dF9xcWW1sz6gbDkuE4K40yKl1tJdfCKg">

        <div class="form-group">
            <label for="email" class="col-md-3 control-label">Название</label>

            <div class="col-md-9">
                <input type="text" class="form-control" name="name_img_edit" value="{{ $file['name'] }}" />
            </div>
        </div>

        <div class="form-group">
            <label for="password" class="col-md-3 control-label">описание</label>

            <div class="col-md-9">
                <textarea class="form-control" name="text_img_edit">{{ $file['text'] }}</textarea>
            </div>
        </div>

        <div class="form-group">
            <label for="password" class="col-md-3 control-label">Порядок сортировки</label>

            <div class="col-md-9">
                <input type="number" class="form-control" name="order_img_edit" value="{{ $file['order'] }}" />
            </div>
        </div>
    </div>
</div>

<div class="modal-footer">
    <button type="button" class="btn btn-primary" onclick="sendEditImg({{ $file['id'] }})">Сохранить</button>
</div>