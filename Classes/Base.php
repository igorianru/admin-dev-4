<?php
namespace App\Classes;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Auth;
use File;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\URL;
use Mail;
use Session;
use App\Modules\Admin\Models\Modules;
use App\Modules\Admin\Models\Right;
use App\Classes\DynamicModel;

class Base
{
    public function __construct(Request $request)
    {
//        parent::__construct();

        $this->request = $request->all();
        $this->requests = $request;
        $this->dynamic = new DynamicModel();
    }

    public static $partnersURI = [
        "/"
    ];
    // авторизованный пользователь
    public static $user;
    //авторизация пользователя, true или false
    public static $authenticate;

    /**
     * инициация авторизированного пользователя, будет доступен везде в Base::$user
     */
    static public function init()
    {
        self::$user = Auth::user();
        self::$authenticate = Auth::check();
    }

    static  $month = [
        '01' => 'Январь',
        '02' => 'Февраль',
        '03' => 'Март',
        '04' => 'Апрель',
        '05' => 'Май',
        '06' => 'Июнь',
        '07' => 'Июль',
        '08' => 'Август',
        '09' => 'Сентябрь',
        '10' => 'Октябрь',
        '11' => 'Ноябрь',
        '12' => 'Декабрь'
    ];

    static  $months = [
        '01' => 'Января',
        '02' => 'Февраля',
        '03' => 'Марта',
        '04' => 'Апреля',
        '05' => 'Мая',
        '06' => 'Июня',
        '07' => 'Июля',
        '08' => 'Августа',
        '09' => 'Сентября',
        '10' => 'Октября',
        '11' => 'Ноября',
        '12' => 'Декабря'
    ];

    static  $day_of_week = [
        '0' => 'Воскресенье',
        '1' => 'Понедельник',
        '2' => 'Вторник',
        '3' => 'Среда',
        '4' => 'Четверг',
        '5' => 'Пятница',
        '6' => 'Суббота'
    ];

    /**
     * редирект назад с сообщением об ошибке
     * @param $message - сообщение об ошике
     * @return mixed
     */
    public static function wrong($message)
    {
        Session::flash('error', $message);
        return redirect()->back();
    }

    /**
     * редирект назад с сообщением
     * @param $message - сообщение
     * @return mixed
     */
    public static function back($message)
    {
        Session::flash('info', $message);
        return redirect()->back();
    }

    /**
     * редирект с сообщением
     * @param $url - путь редиректа
     * @param $message - сообщение
     * @return mixed
     */
    public static function redirect($url, $message)
    {
        Session::flash('info', $message);
        return redirect($url);
    }

    /**
     * формируем склонения
     * @param $number - число, для которого формируем
     * @param $after - массив склонений, например ['день','дня','дней']
     * @return string - возвращаем склонение
     */
    public static function plural($number, $after)
    {
        $cases = array(2, 0, 1, 1, 1, 2);
        return $number . ' ' . $after[($number % 100 > 4 && $number % 100 < 20) ? 2 : $cases[min($number % 10, 5)]];
    }

    /**
     * замена порядкового номера массива на значение из массива
     * @param $name
     * @param $arr
     * @return array
     */
    public static function revertToKey($name, $arr)
    {
        $nevArr = [];
        foreach ($arr as $v) {
            $nevArr[$v[$name]] = $v;
        }

        return $nevArr;
    }

    /**
     * создаёт массив значений
     * @param $name
     * @param $arr
     * @return array
     */
    public static function getArrayVal($name, $arr)
    {
        $nevArr = [];
        foreach ($arr as $v) {
            $nevArr[] = $v[$name];
        }

        return $nevArr;
    }

    /**
     * functions find key by key in array
     * @param $arr
     * @param $key
     * @param $search
     * @return int|string
     */
    public static function findKey($arr, $key, $search) {
        foreach($arr as $k => $v)
            if($v[$key] === $search)
                return $k;
    }

    /**
     * модули|модуль и сразу присваиваем права
     * @param $keySearch - ключ
     * @param $name - с чем сверять
     * @param $list - игнорировать parent и оставить массив одномерным
     * @return mixed
     * admin - если тип юзера (admin) то права будут полные, всегда
     * r - просмотр
     * x - редактирование
     * w - добавление
     * d - удаление
     */
    public static function getModule($keySearch = "link_module", $name = null, $list = false)
    {
        $module = config('admin.module');

        if(Auth::user()->usertype == 'admin')
        {
            foreach($module as $key => $v) {
                if($name === $module[$key][$keySearch] || !$name) {
                    $module[$key] = array_merge($v, ['r' => 1, 'x' => 1, 'w' => 1, 'd' => 1]);

                    if(isset($v['parent'])) {
                        $parent = Base::findKey($module, 'id', $v['parent']);

                        $module[$parent]['m'] = array_merge($module[$parent]['m'] ?? [], [array_merge($module[$key], [
                            'r' => 1,
                            'x' => 1,
                            'w' => 1,
                            'd' => 1
                        ])]);

                        unset($module[$key]);
                    }
                } else {
                    unset($module[$key]);
                }

                if(!$v['active']) unset($module[$key]);
            }
        } else {
            $right = DynamicModel::t('right')
                ->select('id_menu','r', 'x', 'w', 'd')
                ->where('id_user', Auth::user()->id)
                ->whereIn('id_menu', Base::getArrayVal("id", $module))
                ->groupBy('id')
                ->get()
                ->toArray();

            $right = Base::revertToKey("id_menu", $right);

            foreach($module as $key => $v) {
                if($name === $module[$key][$keySearch] || !$name) {
                    if(Auth::user()->usertype == 'admin')
                    {
                        $module[$key] = array_merge($v, ['r' => 1, 'x' => 1, 'w' => 1, 'd' => 1]);
                    } else {
                        if(!isset($right[$v['id']])) {
                            $module[$key] = array_merge($v, ['r' => 0, 'x' => 0, 'w' => 0, 'd' => 0]);
                        } else {
                            $module[$key] = array_merge(['r' => 1, 'x' => 1, 'w' => 1, 'd' => 1], $v, $right[$v['id']]);
                        }
                    }

                    if(isset($v['parent'])) {
                        $parent = Base::findKey($module, 'id', $v['parent']);
                        $module[$parent]['m'] = array_merge($module[$parent], [
                            array_merge($module[$key], [
                                'r' => $module[$key]['r'],
                                'x' => $module[$key]['x'],
                                'w' => $module[$key]['w'],
                                'd' => $module[$key]['d']
                            ])]);

                        unset($module[$key]);
                    }
                } else {
                    unset($module[$key]);
                }

                if(!$v['active']) unset($module[$key]);
            }
        }

        // sorting to order
        usort($module, function($a, $b){
            return $a['order'] <=> $b['order'];
        });

        return isset($module[0]['m']) ? $module[0]['m'] : $module;
    }

    /**
     * Отправляет письмо
     * @param $layout - шаблон письма
     * @param $user - модель пользователя
     * @param $password - пароль пользователя
     */
    public static function mail($layout, $user, $password)
    {
        Mail::send('emails.' . $layout, ['user' => $user, 'password' => $password], function ($m) use ($user) {
            $m->from('igorian.ru@mail.ru', 'Служба техподдержки');
            $m->to($user->email, $user->name)->subject('Создание аккаунта');
        });
    }

    /**
     * логирование внутренних ошибок
     * $err - ошибка, класс Exception
     * @param \Exception $err
     */
    public static function logError(\Exception $err)
    {
        $file = '../storage/logs/system-error.log';
        error_log('Date: ' . date('Y-m-d H:m:s', time()) . "\n", 3, $file);
        error_log('Error message: ' . $err->getMessage() . "\n", 3, $file);
        error_log('File: ' . $err->getFile() . "\n", 3, $file);
        error_log('Line: ' . $err->getLine() . "\n\n", 3, $file);
    }

    /**
     * @param \Exception $err
     * @return \Illuminate\View\View
     */
    public static function errorPage(\Exception $err)
    {
        Log::error('Error: ' . $err->getMessage());
        Log::error('File: ' . $err->getFile() . ' at line ' . $err->getLine());
        return self::view('admin/dashboard/index');
    }

    /**
     * формируем вьюху, с обязательными параметрами
     * @param $url - путь вьюхи
     * @param array $args - аргументы
     * @param $url
     * @param array $args
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public static function view($url, $args = [])
    {
        if(Auth::user())
        {
            $args['segment3'] = \Request::segment(3);
            $args['left_menu'] = Base::getModule();
        }

        $args['version'] = '4.2.0';

        $args['status_app_0'] = DynamicModel::t('applications')
            ->select(DB::raw('COUNT(status_app) as r'))
            ->where(['status_app' => 0])
            ->first()
            ->toArray();

        $args['status_app_1'] = DynamicModel::t('applications')
            ->select(DB::raw('COUNT(status_app) as r'))
            ->where(['status_app' => 1])
            ->first()
            ->toArray();

        $args['status_app_2'] = DynamicModel::t('applications')
            ->select(DB::raw('COUNT(status_app) as r'))
            ->where(['status_app' => 2])
            ->first()
            ->toArray();

        return view($url, $args);
    }

    /**
     * is JSON
     * @param $string
     * @return bool
     */
    static function is_json($string) {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }

    /**
     * формируем вьюху, с обязательными параметрами
     * @param $url - путь вьюхи
     * @param array $args - аргументы
     * @param $url
     * @param array $args
     * @param string $menu_mame
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view_s($url, $args = [], $menu_mame = 'menu')
    {
        $args['menu'] = $this->get_cat($menu_mame, ['active' => 1]);

        /**/
        // при использовании языков идёт смещение сегментов урла
        if(\App::getLocale() == $this->requests->segment(1)) {
            $segment1 = $this->requests->segment(2);
            $segment2 = $this->requests->segment(3);
            $segment3 = $this->requests->segment(4);
        } else {
            $segment1 = $this->requests->segment(1);
            $segment2 = $this->requests->segment(2);
            $segment3 = $this->requests->segment(3);
        }

        if($segment3 && $segment1 != 'm')
            $args['data'] = $this->dynamic->t('str')->where(['active' => 1, 'cat' => $segment3])->get()->toArray();
        else {
            if(isset($args['field']))
            {
                if($segment2 == '')
                    $text =  htmlspecialchars(urldecode($segment1));
                else
                    $text = htmlspecialchars(urldecode($segment2));

                if($segment1 == 'm' && $segment3) $text = htmlspecialchars(urldecode($segment3));
                $menu = $this->dynamic->t($menu_mame)->where(['active' => 1, 'translation' => $text])->first();

                $args['data'] = $this
                    ->dynamic
                    ->t('str')
                    ->where(['active' => 1, $args['field'] => $text])
                    ->get()
                    ->toArray();

                if(empty($args['data'])) $args['data'][0] = $menu;
            } else {
                if($segment2) {
                    $menu = $this->dynamic->t($menu_mame)->where(['active' => 1, 'id' => $segment2])->first();

                    $args['data'] = $this
                        ->dynamic
                        ->t('str')
                        ->where(['active' => 1, 'cat' => $segment2])
                        ->get()
                        ->toArray();

                    if(empty($args['data']))
                        $args['data'][0] = $menu;
                } else {
                    $menu = $this->dynamic->t($menu_mame)->where(['active' => 1, 'translation' => $segment1])->first();

                    if(!empty($menu)) {
                        $args['data'] = $this
                            ->dynamic
                            ->t('str')
                            ->where(['active' => 1, 'cat' => $menu->id])
                            ->get()
                            ->toArray();
                    }

                    if(empty($args['data']))
                        $args['data'][0] = $menu;
                }
            }
        }

        if(!empty($args['data']))
        {
            if($this->is_json($args['data'][0]['title'])) {
                $args['meta']['title'] = json_decode($args['data'][0]['title'], true)[\App::getLocale()] ?? '';
                $args['meta']['description'] = json_decode($args['data'][0]['description'], true)[\App::getLocale()] ?? '';
                $args['meta']['keywords'] = json_decode($args['data'][0]['keywords'], true)[\App::getLocale()] ?? '';
            } else {
                $args['meta']['title'] = $args['data'][0]['title'];
                $args['meta']['description'] = $args['data'][0]['description'];
                $args['meta']['keywords'] = $args['data'][0]['keywords'];
            }
        }

        if(isset($args['meta'])) {
            if(!$segment1)
                $segment1 = 'main';

            $dat = $this->dynamic->t('seo')->where(['name' => $segment1])->get()->toArray();

            if(!empty($dat)) {
                $args['meta']['title'] = $dat[0]['title'];
                $args['meta']['description'] = $dat[0]['description'];
                $args['meta']['keywords'] = $dat[0]['keywords'];
            }
        }

        $param = $this->dynamic->t('params')->select('params.*', 'little_description as key')->get();

        foreach ($param as $key => $p )
            $args['params'][$p->name] = $p->toArray();

        if(isset($args['meta_c']))
            $args['meta'] = $args['meta_c'];

        $start = 5;
        $end = $start + 2;
        $args['range_time'] = [];

        for($i = 0; $i < 9; $i++) {
            $active = true;
            $start = $start + 2;
            $end   = $end + 2;

            if($end < (int) Carbon::now()->hour)
                $active = false;

            if($start > 24)
                $start = $start - 24;

            if($end > 24)
                $end = $end - 24;

            $args['range_time'][] = ['start' => $start . ':00', 'end' => $end . ':00', 'active' => $active];
        }

        $args['address'] = $this->requests->session()->get('address');
        $args['time_current'] = $this->requests->session()->get('time_current');
        $args['time_current_month'] = $this->requests->session()->get('time_current_month');
        $args['time_current_day'] = $this->requests->session()->get('time_current_day');

        return view($url, $args);
    }

    /**
     * @param $message
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public static function returnJSON($message)
    {
        return response()->json(['message' => $message]);
    }


    /**
     * @param $arr
     * @param int $cat
     * @param int $i
     * @param int $w
     * @param $s
     * @param string $menu
     * @return string
     */
    public function view_cat($arr,$cat = 0, $i = 0, $w = 0, $s, $menu = '')
    {
        if(empty($arr[$cat])) {
            return $menu;
        }

        if($cat != 0) {
            $r = 'display:';
        } else {
            $r = '';
        }

        if(isset($arr[$cat][$i]['cat'])) {
            $f = $arr[$cat][$i]['cat'];
        } else {
            $f = '';
        }

        if($w == 1)
        {
            echo '<ul class=" top-menu-s">';
        }

        //перебираем в цикле массив и выводим на экран
        for($i = 0; $i < count($arr[$cat]);$i++)
        {

            $arr[$cat][$i]['an_link'] =  explode('?v=off', $arr[$cat][$i]['an_link'])[0];

            if($arr[$cat][$i]['cat'] > 0 || $arr[$cat][$i]['an_link'] == $s)
            {
                if($arr[$cat][$i]['an_link'] != $s)
                {
                    if($arr[$cat][$i]['sys_cat'] != '')
                    {
                        $l = '/'.$s.'/'.$arr[$cat][$i]['sys_cat'].'';
                    } else {
                        $l = '/'.$s.'/page/'.$arr[$cat][$i]['id'].'';
                    }
                    if($arr[$cat][$i]['an_link'] != '') {
                        $l = $arr[$cat][$i]['an_link'];
                    }

                    if($i == 0) {
                        $cl = 'first';
                    } else {
                        if($i+1 == count($arr[$cat])) {
                            $cl = 'last';
                        } else {
                            $cl = '';
                        }
                    }

                    echo '<li  role="presentation"><a href="'.$l.'"  class="'.$cl.'">'
                        .$arr[$cat][$i]['name'].'</a>';
                    //рекурсия - проверяем нет ли дочерних категорий
                    $this->view_cat($arr,$arr[$cat][$i]['id'], $i, 1, $s, $menu);
                    echo '</li>';
                } else {
                    //рекурсия - проверяем нет ли дочерних категорий
                    $this->view_cat($arr,$arr[$cat][$i]['id'], $i, 0, $s, $menu);
                }
            }

        }

        if($w == 1)
        {
            echo '</ul>';
        }
    }


    public function _menu_site_select($where = [], $array = null, $cat = 0, $parent = 0, $level = 0, $table = 'menu')
    {
        if(!$array)
            $array = $this->dynamic->t($table)->where($where)->get()->toArray();

        $l = array('', '---', '-------', '-----------', '--------------', '-----------------', '----------------------');
        $return = '';
        $left = $l[$level];

        foreach ($array as $item) {
            if ($cat == $item['id']) {
                $t = 'selected';
            } else {
                $t = '';
            }

            if ($item['cat'] != $parent) continue;
            $return .= '<option value="' . $item['id'] . '" ' . $t . '> ' . $left . ' ' . $item['name'] . '</option>';
            $return .= $this->_menu_site_select($where, $array, $cat, $item['id'], $level + 1);//вывод вложенных записей
        }

        return $return . '';
    }

    public function get_cat_c($where, $cat = '', $table = 'menu')
    {
        $array = $this->dynamic->t($table)
            ->where($where)
            ->get()->toArray();

        if (!$array) {
            return NULL;
        }

        $arr_cat = array();

        //В цикле формируем массив
        for ($i = 0; $i < count($array); $i++) {
            $row = $array[$i];

            //Формируем массив, где ключами являются адишники на родительские категории
            if (empty($arr_cat[$row['cat']])) {
                $arr_cat[$row['cat']] = array();
            }

            $arr_cat[$row['cat']][] = $row['id'];
        }

        $arr = array();
        if(isset($arr_cat[$cat]))
        {
            $arr = array_merge($arr, $arr_cat[$cat]);
            for($i = 0; $i < count($arr_cat[$cat]); $i++)
            {
                if(isset($arr_cat[$cat][$i]))
                {
                    for($ii = 0; $ii < count($arr_cat[$cat][$i]); $ii++)
                    {
                        $id = $arr_cat[$cat][$i];

                        if(isset($arr_cat[$id]))
                        {

                            if(isset($arr_cat[$id][$ii]))
                            {
                                for($iii = 0; $iii < count($arr_cat[$id][$ii]); $iii++)
                                {
                                    $id2 = $arr_cat[$id][$ii];

                                    if(isset($arr_cat[$id2]))
                                    {

                                        $arr = array_merge($arr, $arr_cat[$id2]);
                                    } /*else {
											$arr = array_push($arr, $arr_cat[$id2][$iii]);
										}*/
                                }
                            } else {
                                $arr = array_push($arr, $arr_cat[$id][$ii]);
                            }

                            $arr = array_merge($arr, $arr_cat[$id]);
                        }/* else {
								$arr = array_push($arr, $arr_cat[$id]);
							}*/
                    }
                } else {
                    $arr = array_push($arr, $arr_cat[$cat][$i]);
                }
            }
        } else {
            $arr = [$cat];
        }

        return $arr;
    }

    /**
     * проверка/получение прав пользователя
     * @param null $table
     * @return mixed
     */
    public function right($table = null)
    {
        if($table) {
            $segment3 = $table;
        } else {
            $segment3 = $this->requests->segment(3);
            $segment2 = $this->requests->segment(2);

            if(!$segment3 || $segment2 == 'settings') {
                $segment3 = $segment2;
            }
        }

        return $this->getModule("link_module", $segment3)[0];
    }

    /**
     * формируем массив меню для вывода деревом
     * @param null $table
     * @param array $where
     * @param string $order
     * @param string $sort
     * @return array|null
     */
    public function get_cat($table = null, $where = [], $order = 'order', $sort = 'ASC')
    {
        if(!$table) $table = 'menu';
        $data = $this->dynamic->t($table)->where($where)->orderBy($order, $sort)->get();
        if (!$data) return NULL;

        $arr_cat = array();
        if (!empty($data)) {

            //В цикле формируем массив
            for ($i = 0; $i < count($data); $i++) {
                $row = $data[$i];

                //Формируем массив, где ключами являются адишники на родительские категории
                if (empty($arr_cat[$row['cat']])) {


                    $arr_cat[$row['cat']] = array();
                }

                $arr_cat[$row['cat']][] = $row->toArray();
            }

            //возвращаем массив
            return $arr_cat;
        } else {
            return [];
        }
    }

    /**
     * проверка прав
     * @return bool
     */
    public function right_check()
    {
        $right = $this->right();

        if(empty($right)) {
            // создане прав
        } else {
            $seg = $this->requests->segments()[1];
            $r = true;

            switch ($seg) {
                case 'index':
                    // добавление
                    if(!$right['r']) {
                        $r = false;
                    }
                    break;

                case 'update':
                    if(isset($this->requests->segments()[3]))
                    {
                        // редактирование
                        if(!$right['x']) {
                            $r = false;
                        }
                    } else {
                        // добавлени
                        if(!$right['w']) {
                            $r = false;
                        }
                    }
                    break;

                case 'delete':
                    // удаление
                    if(!$right['d']) {
                        $r = false;
                    }
                    break;

                default:
                    $r = true;
            }

            if(!$r) {
                abort(503, 'Недостаточно прав');
            } else {
                return $r;
            }
        }
    }

    /**
     * function for transform to one-dimensional array
     * @param $arr
     * @return array|bool
     */
    public function makeSingleArray($arr)
    {
        if (!is_array($arr)) return false;
        $tmp = array();

        $arr = (isset($arr['offer'][0])) ? $arr['offer'] : [$arr['offer']];

        foreach ($arr as $val) {
            $tmp[] = $this->_makeSingleArray($val, '');
        }

        return $tmp;
    }

    public function _makeSingleArray($arr, $key_arr = '')
    {
        if (!is_array($arr)) return false;
        $tmp = array();
        $i = 0;

        foreach ($arr as $key => $val) {
            if (is_array($val) && isset($val[0]) == false) {
                $tmp = array_merge($tmp, $this->_makeSingleArray($val, $key));
            } else {
                $key_arr_p = ($key_arr && !$i) ? '_' . $key_arr : '';
                $tmp[$key . $key_arr_p] = $val;
            }

            $i++;
        }

        return $tmp;
    }
}
