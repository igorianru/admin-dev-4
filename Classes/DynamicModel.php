<?php

namespace App\Classes;

use Illuminate\Database\Eloquent\Model as EloquentModel;
use Illuminate\Support\Facades\DB;

class DynamicModel extends EloquentModel
{
    /**
     * @var
     */
    protected static $_table;

    /**
     * @param $table
     * @param array $parms
     * @return null|static
     */
    public static function t($table, $parms = Array())
    {
        $ret = null;

        if (class_exists($table)) {
            $ret = new $table($parms);
        } else {
            $ret = new static($parms);
            $ret->setTable($table);
        }
        return $ret;
    }

    /**
     * @param string $table
     * @return string
     */
    public function setTable($table)
    {
        return static::$_table = $table;
    }

    /**
     * @return mixed
     */
    public function getTable()
    {
        return static::$_table;
    }

    /**
     * @return mixed
     */
    public function getAllTableName()
    {
        $table = DB::select('SHOW TABLES');
        $arr   = [];

        foreach ($table as $key => $val) {
            $val = (array)$val;
            $arr[] = array_shift($val);
        }

        return $arr;
    }


    /**
     * @param null $name
     * @return array
     */
    public function getAllColumnTableName($name = null)
    {
        $arr = [];

        if($name) {
            $table = DB::select('SHOW FIELDS FROM ' . $name);

            foreach ($table as $key => $val) {
                $val = (array)$val;
                $arr[] = array_shift($val);
            }
        }

        return $arr;
    }

    /**
     * @return mixed
     */
    public function rand()
    {
        return DB::raw('RAND()');
    }
}
